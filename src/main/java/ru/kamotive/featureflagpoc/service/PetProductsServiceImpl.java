package ru.kamotive.featureflagpoc.service;


import org.springframework.stereotype.Service;

@Service("petProductsServiceImpl")
public class PetProductsServiceImpl implements PetProductsService {

    @Override
    public String getPetProductsString(String name) {
        System.out.println("We are triggering PET_PRODUCTS_SERVICE_IMPL");
        System.out.println(name);
        return "GENERAL PET PRODUCTS SERVICE IMPLEMENTATION";
    }

}
