package ru.kamotive.featureflagpoc.service;

import org.springframework.stereotype.Service;

@Service("petPrescriptionServiceImpl")
public class PetPrescriptionServiceImpl implements PetProductsService {

    @Override
    public String getPetProductsString(String name) {
        System.out.println("We are triggering PET_PRESCRIPTION_SERVICE_IMPL");
        System.out.println(name);
        return "PET PRESCRIPTION SERVICE IMPLEMENTATION";
    }

}
