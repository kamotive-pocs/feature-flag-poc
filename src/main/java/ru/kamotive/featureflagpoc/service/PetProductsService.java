package ru.kamotive.featureflagpoc.service;

import org.unleash.features.annotation.Toggle;

public interface PetProductsService {

    @Toggle(name = "productsPageFlag", alterBean = "petPrescriptionServiceImpl")
    String getPetProductsString(String name);

}