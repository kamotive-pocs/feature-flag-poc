package ru.kamotive.featureflagpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeatureFlagPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeatureFlagPocApplication.class, args);
    }

}
