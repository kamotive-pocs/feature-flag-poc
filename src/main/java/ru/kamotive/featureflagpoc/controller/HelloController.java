package ru.kamotive.featureflagpoc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/api/v1")
    public String index() {
        System.out.println("We are triggering HELLO_CONTROLLER");
        return "Greetings from Spring Boot!";
    }
}