package ru.kamotive.featureflagpoc.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kamotive.featureflagpoc.service.PetProductsService;

@RestController
class PetProductsController {

    private final PetProductsService petProductsService;

    public PetProductsController(@Qualifier("petProductsServiceImpl") PetProductsService petProductsService) {
        this.petProductsService = petProductsService;
    }

    @GetMapping("/flagtest")
    public String loadProductsPage() {
        return petProductsService.getPetProductsString("VIEWING CLINIC PRODUCTS PAGE");
    }
}